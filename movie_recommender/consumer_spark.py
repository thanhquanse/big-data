from __future__ import print_function
import os
from pyspark.sql import SparkSession
from pyspark.streaming import StreamingContext
from model.MovieRecommender import MovieRecommender
from producer import MovieRSProducer

os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.1,' \
                                    'org.apache.spark:spark-streaming-kafka-0-10-assembly_2.12:3.1.1 pyspark-shell'
BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
movies_data = os.path.join(BASE_DIR, "data/movies.csv")
rating_data = os.path.join(BASE_DIR, "data/ratings.csv")

# Loading model globally
Recommender = MovieRecommender(movies_data, rating_data)

# Initialize Response topic
Producer = MovieRSProducer(bootstrap_server="35.225.134.239:9092", topic="MovieRSResponse")


class MovieRSConsumer:
    def __init__(self, bootstrap_server="localhost:9092", topic="MovieRS"):
        self.bootstrap_server = bootstrap_server
        self.topic = topic
        self.sc = SparkSession.builder.appName("MovieRS").getOrCreate()

    def predict_by_user(self, user_id, *args, **kwargs):
        # print(args)
        results = []
        user_list = user_id.select('value').rdd.flatMap(lambda x: x).collect()
        for id in user_list:
            if id:
                results = Recommender.recommend_sim_MF_for_user(int(id))
                Producer.produce_message(results)
        print(results)


    def consume_message(self):
        # self.sc.register("MovieRS", self.predict_by_user)
        StreamingContext(self.sc, 60)
        dataframe = self.sc.readStream.format("kafka").option("kafka.bootstrap.servers", self.bootstrap_server).\
            option("subscribe", self.topic).load().selectExpr("CAST(value AS STRING)")

        # dataframe.select("value").writeStream.format("console").outputMode("append").start().awaitTermination()
        dataframe.select("value").writeStream.foreachBatch(self.predict_by_user).start().awaitTermination()


if __name__ == "__main__":
    Consumer = MovieRSConsumer(bootstrap_server="35.225.134.239:9092", topic="MovieRSRequest")
    Consumer.consume_message()
    # print(Recommender.recommend_sim_MF_for_user(1))
    # consumer = KafkaConsumer("MovieRSRequest", bootstrap_servers="35.225.134.239:9092", auto_offset_reset="earliest")
    # for mgs in consumer:
    #     print(mgs.value.decode("utf-8"))
