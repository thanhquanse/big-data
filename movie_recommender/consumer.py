from __future__ import print_function
import os
import time

from kafka import KafkaConsumer
from model.MovieRecommender import MovieRecommender
from producer import MovieRSProducer

BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
movies_data = os.path.join(BASE_DIR, "data/movies.csv")
rating_data = os.path.join(BASE_DIR, "data/ratings.csv")

# Loading model globally
Recommender = MovieRecommender(movies_data, rating_data)

# Initialize Response topic
Producer = MovieRSProducer(bootstrap_server="35.225.134.239:9092", topic="MovieRSResponse")


class MovieRSConsumer:
    def __init__(self, bootstrap_server="localhost:9092", topic="MovieRS"):
        self.bootstrap_server = bootstrap_server
        self.topic = topic

    def predict_by_user(self, user_id, *args, **kwargs):
        # print(args)
        results = []
        # user_list = user_id.select('value').rdd.flatMap(lambda x: x).collect()
        # for id in user_list:
        #     if id:
        results = Recommender.recommend_sim_MF_for_user(int(user_id))
        Producer.produce_message(results)
        print(results)

    def consume_message(self):
        consumer = KafkaConsumer("MovieRSRequest", bootstrap_servers="35.225.134.239:9092", auto_offset_reset="latest")
        for mgs in consumer:
            if mgs:
                user_id = mgs.value.decode("utf-8")
                print(f"Processing user id {user_id}...")
                self.predict_by_user(user_id)


if __name__ == "__main__":
    print("Starting Movie RS Kafka consumer...")
    print("Waiting for message...")
    Consumer = MovieRSConsumer(bootstrap_server="35.225.134.239:9092", topic="MovieRSRequest")
    while True:
        Consumer.consume_message()

