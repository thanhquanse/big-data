import os
# from ..recommender.MovieRecommender import MovieRecommender
from ..recommender.Recommender import MovieRecommender
from ..services import MovieService as mvs
from ..predict import Predict
from movie_recommender.settings import BASE_DIR

movies_data = os.path.join(BASE_DIR, "movieRS/data/movies.csv")
rating_data = os.path.join(BASE_DIR, "movieRS/data/ratings.csv")
database = os.path.join(BASE_DIR, "db.sqlite3")

recommender = MovieRecommender(movies_data, rating_data, database)


def getRecommendedMoviesForUser(userID):
    if mvs.is_user_existing(userID):
        return recommender.recommend_sim_MF_for_user(userID)
    return None

def getRecommendedMoviesForUser2(userID):
    return Predict.recommend_movie(userID)

def getRatedMovie(userID):
    return Predict.get_rated_movies(userID)

def getWatchingSimilarMovies(movieID):
    return recommender.recommend_sim_MF(movieID)


def getColdStartMovies(search_text=None):
    if search_text:
        return recommender.recommend_sim_cold_start(search_text)
    else:
        return recommender.recommend_sim_cold_start_rand()

def getRatingOfMovie(movieId):
    return mvs.getListRatingOfMovie(movieId)


def getAvgRatingOfMovie(movieId):
    return mvs.getAvgRatingOfMovie(movieId)


def getRatingOfMovieByUser(movieID, userID):
    return mvs.getRatingByUser(movieID, userID)


def updateOrCreate(movieID, userID, rating):
    return recommender.update_model_matrix(movieID, userID, rating)


def getRatingListByUser(userID):
    return mvs.getRatingListByUser(userID)
