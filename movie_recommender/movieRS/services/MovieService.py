from ..models.models import User, Rating


def is_user_existing(userid):
    try:
        exist = User.objects.filter(user_id=str(userid)).exists()
        if exist:
            print("Found user: ", userid)
        else:
            print("Not found user: ", userid)
    except:
        exist = False
        print("Not found user: ", userid)
    return exist


def getListRatingOfMovie(movieID):
    ratings = []
    try:
        ratings = Rating.objects.filter(movie_id=movieID).values('user_id', 'rating')
    # serializers = RatingSerializer(data=ratings)
    # if serializers.is_valid():
    #     print(serializers.data)
    except:
        print("Error in getting rating list")
    return ratings


def getAvgRatingOfMovie(movieID):
    avg = 0
    try:
        ratings = Rating.objects.filter(movie_id=movieID).values('user_id', 'rating')
        sumOfRate = 0
        for rate in ratings:
            sumOfRate = sumOfRate + rate['rating']
        avg = round(sumOfRate / len(ratings), 1)
    except:
        print("Error in getting rating list")
    return avg


def getRatingListByUser(userID):
    ratings = []
    try:
        ratings = Rating.objects.filter(user_id=userID).values('user_id', 'movie_id', 'rating')
    except:
        print("Error in getting rating list")
    return ratings


def getRatingByUser(movieID, userID):
    rating = 0
    try:
        rating = Rating.objects.get(movie_id=str(movieID), user_id=str(userID)).rating
    except:
        print("Error: Rating object not found")

    return rating
