import csv
import os
import django
import sys
from pathlib import Path

sys.path.append(os.path.join(Path(__file__).resolve().parent.parent.parent.parent, 'movie_recommender'))
sys.path.append(os.path.join(Path(__file__).resolve().parent.parent.parent, 'movieRS'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "movie_recommender.settings")
django.setup()

from movieRS.models.models import Rating, Movie, User
from movie_recommender.settings import BASE_DIR

movies_data = os.path.join(BASE_DIR, "movieRS/data/movies.csv")
rating_data = os.path.join(BASE_DIR, "movieRS/data/ratings.csv")

if __name__ == "__main__":
    print("Importing csv data into database...")
    with open(movies_data, 'rt') as f:
        reader = csv.reader(f, dialect='excel')
        next(reader, None)
        for row in reader:
            if Movie.objects.filter(movieID=row[0]).exists():
                print("Movie is existing: ", row[0])
            else:
                print("Importing movie: ", row[0], row[1])
                movie = Movie.objects.create(movieID=row[0], title=row[1])
                movie.save()

    with open(rating_data, 'rt') as f:
        reader = csv.reader(f, dialect='excel')
        next(reader, None)
        for row in reader:
            if Rating.objects.filter(user_id=row[0], movie_id=row[1]).exists():
                print("Rating is existing: ", row[0], row[1], row[2])
            else:
                print("Importing user and rating: ", row[0], row[1], row[2])
                if not User.objects.filter(userID=row[0]).exists():
                    user = User.objects.create(userID=row[0], username=row[0])
                    user.save()
                # Get user just imported above
                user = User.objects.get(userID=row[0])
                # Get movie just imported above
                movie = Movie.objects.get(movieID=row[1])
                rating = Rating.objects.create(user=user, movie=movie, rating=row[2])
                rating.save()
