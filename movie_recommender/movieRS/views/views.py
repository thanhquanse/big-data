from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from ..models.models import Movie, User, Rating
from .serializers import UserSerializer, MovieSerializer, RatingSerializer
from ..controllers import MovieController as mvc


# Create your views here.

class UserListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = {
            'userID': request.data.get('userID'),
            'username': request.data.get('username'),
            'full_name': request.data.get('full_name')
        }
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MovieListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        movies = Movie.objects.all()
        serializer = MovieSerializer(movies, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = {
            'movieID': request.data.get('movieID'),
            'title': request.data.get('title'),
            'genre': request.data.get('genre')
        }
        serializer = MovieSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MovieWatchingRecommendedListAPIView(APIView):
    def get(self, request, movieId, *args, **kwargs):
        movies = mvc.getWatchingSimilarMovies(movieId)
        # serializer = MovieNameSerializer(movies, many=True)

        return Response(movies, status=status.HTTP_200_OK)


class MovieRecommendedForUserListAPIView(APIView):
    def get(self, request, userId, *args, **kwargs):
        movies = mvc.getRecommendedMoviesForUser2(userId)
        print(movies)
        return Response(movies, status=status.HTTP_200_OK)


class ColdStartMovieListAPIView(APIView):
    def get(self, request, search_text=None, *args, **kwargs):
        movies = mvc.getColdStartMovies(search_text)
        return Response(movies, status=status.HTTP_200_OK)


class RatingListOfMovieAPIView(APIView):
    def get(self, request, movieId, *args, **kwargs):
        ratings = mvc.getRatingOfMovie(movieId)
        return Response(ratings, status=status.HTTP_200_OK)


class AvgRatingOfMovieAPIView(APIView):
    def get(self, request, movieId, *args, **kwargs):
        avg = mvc.getAvgRatingOfMovie(movieId)
        return Response(avg, status=status.HTTP_200_OK)


class RatingOfMovieByUserAPIView(APIView):
    def get(self, request, movieId, userId, *args, **kwargs):
        rating = mvc.getRatingOfMovieByUser(movieId, userId)
        return Response(rating, status=status.HTTP_200_OK)


class RatingMovieListByUser(APIView):
    def get(self, request, userId, *args, **kwargs):
        rating_list = mvc.getRatingListByUser(userId)
        return Response(rating_list, status=status.HTTP_200_OK)

class RatedMovieListByUser(APIView):
    def get(self, request, userId, *args, **kwargs):
        movie_dict_list = mvc.getRatedMovie(userId)
        return Response(movie_dict_list, status=status.HTTP_200_OK)

class RatingMovieUpdateAPIView(APIView):
    # def get(self, request, *args, **kwargs):
    #     movieID = request.data.get("movieID")
    #     userID = request.data.get("userID")
    #     rating = request.data.get("rating")
    #
    #     return Response(mvc.getRating(movieID, userID), status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        movieID = request.data.get("movieID")
        userID = request.data.get("userID")
        rating = request.data.get("rating")
        data = {
            'movie_id': str(movieID),
            'user_id': str(userID),
            'rating': rating,

        }
        print(request.data.get("movieID"))
        print(request.data.get("userID"))
        try:
            obj = Rating.objects.get(movie_id=str(movieID), user_id=str(userID))
            serializer = RatingSerializer(obj, data=data, partial=True)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                mvc.updateOrCreate(movieID, userID, rating)
                return Response(data=serializer.data, status=status.HTTP_200_OK)
        except:
            print("Error: Rating object not found")
        return Response(status=status.HTTP_400_BAD_REQUEST)
