from rest_framework import serializers
from ..models.models import User, Movie, Rating


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'


class MovieNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ['title']


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = '__all__'

    # def update(self, instance, validated_data):
    #     instance.rating = validated_data.get('your field name', 'defaultvalue')
    #     instance.save()
    #     return instance
