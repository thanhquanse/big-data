from django.urls import path, include
from .views.views import RatedMovieListByUser, RatingMovieListByUser, RatingOfMovieByUserAPIView, AvgRatingOfMovieAPIView, RatingMovieUpdateAPIView, RatingListOfMovieAPIView, ColdStartMovieListAPIView, MovieWatchingRecommendedListAPIView, MovieRecommendedForUserListAPIView, MovieListAPIView, UserListAPIView

urlpatterns = [
    # path('', views.index, name='index')
    path('api/v1/movies/', MovieListAPIView.as_view()),
    path('api/v1/users/', UserListAPIView.as_view()),
    path('api/v1/movies/watching/<int:movieId>/', MovieWatchingRecommendedListAPIView.as_view()),
    path('api/v1/movies/recommended_for/<int:userId>/', MovieRecommendedForUserListAPIView.as_view()),
    path('api/v1/movies/cold_start/', ColdStartMovieListAPIView.as_view()),
    path('api/v1/movies/cold_start/<str:search_text>/', ColdStartMovieListAPIView.as_view()),
    path('api/v1/movies/ratings/<int:movieId>/', RatingListOfMovieAPIView.as_view()),
    path('api/v1/movies/ratings/<int:movieId>/<int:userId>/', RatingOfMovieByUserAPIView.as_view()),
    path('api/v1/movies/ratings/avg/<int:movieId>/', AvgRatingOfMovieAPIView.as_view()),
    path('api/v1/movies/ratings/update/', RatingMovieUpdateAPIView.as_view()),
    path('api/v1/movies/ratings/user/<int:userId>/', RatingMovieListByUser.as_view()),
    path('api/v1/movies/rated/user/<int:userId>/', RatedMovieListByUser.as_view())
]
