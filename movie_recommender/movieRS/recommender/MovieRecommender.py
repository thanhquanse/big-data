from ..models.models import User, Movie
import pandas as pd
import numpy as np
import random
from scipy.sparse import csr_matrix
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.decomposition import TruncatedSVD
from sklearn.neighbors import NearestNeighbors


class MovieRecommender:
    def __init__(self, movieDS=None, ratingDS=None):
        self.movieDS = movieDS
        self.ratingDS = ratingDS

    def get_model(self):
        ratings = pd.read_csv(self.ratingDS)
        movies = pd.read_csv(self.movieDS)
        # print(ratings.head())

        X, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper = self.create_X(ratings)
        n_ratings_per_user = X.getnnz(axis=1)
        n_ratings_per_movie = X.getnnz(axis=0)
        # Normalizing our data
        sum_ratings_per_movie = X.sum(axis=0)
        mean_rating_per_movie = sum_ratings_per_movie / n_ratings_per_movie
        X_mean_movie = np.tile(mean_rating_per_movie, (X.shape[0], 1))
        X_norm = X - csr_matrix(X_mean_movie)
        return X, X_norm, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper, movies, ratings

    def create_X(self, df):
        """
            Generates a sparse matrix from ratings dataframe.

            Args:
                df: pandas dataframe containing 3 columns (userId, movieId, rating)

            Returns:
                X: sparse matrix
                user_mapper: dict that maps user id's to user indices
                user_inv_mapper: dict that maps user indices to user id's
                movie_mapper: dict that maps movie id's to movie indices
                movie_inv_mapper: dict that maps movie indices to movie id's
            """
        M = df['userId'].nunique()
        N = df['movieId'].nunique()

        user_mapper = dict(zip(np.unique(df["userId"]), list(range(M))))
        movie_mapper = dict(zip(np.unique(df["movieId"]), list(range(N))))

        user_inv_mapper = dict(zip(list(range(M)), np.unique(df["userId"])))
        movie_inv_mapper = dict(zip(list(range(N)), np.unique(df["movieId"])))

        user_index = [user_mapper[i] for i in df['userId']]
        item_index = [movie_mapper[i] for i in df['movieId']]

        X = csr_matrix((df["rating"], (user_index, item_index)), shape=(M, N))

        return X, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper


    def find_similar_movies(self, movie_id, X, movie_mapper, movie_inv_mapper, k, metric='cosine'):
        """
        Finds k-nearest neighbours for a given movie id.

        Args:
            movie_id: id of the movie of interest
            X: user-item utility matrix
            k: number of similar movies to retrieve
            metric: distance metric for kNN calculations

        Output: returns list of k similar movie ID's
        """
        X = X.T
        neighbour_ids = []

        movie_ind = movie_mapper[movie_id]
        movie_vec = X[movie_ind]
        if isinstance(movie_vec, (np.ndarray)):
            movie_vec = movie_vec.reshape(1,-1)
        # use k+1 since kNN output includes the movieId of interest
        kNN = NearestNeighbors(n_neighbors=k+1, algorithm="brute", metric=metric)
        kNN.fit(X)
        neighbour = kNN.kneighbors(movie_vec, return_distance=False)
        for i in range(0,k):
            n = neighbour.item(i)
            neighbour_ids.append(movie_inv_mapper[n])
        neighbour_ids.pop(0)
        return neighbour_ids


    def movie_finder(self, title, movies):
        return movies[movies['title'].str.contains(title)]['title'].tolist()

    def recommend_sim_knn_movies(self, movie_id):
        movie_names = []
        X, X_norm, user_mapper, movie_mapper, _, movie_inv_mapper, movies, _ = self.get_model()
        movie_titles = dict(zip(movies['movieId'], movies['title']))

        similar_movies = self.find_similar_movies(movie_id, X_norm, movie_mapper, movie_inv_mapper, metric='cosine', k=10)
        movie_title = movie_titles[movie_id]

        print(f"*** Because you watched {movie_title}:")
        for i in similar_movies:
            print(movie_titles[i])
            movie_names.append(movie_titles[i])
        return movie_names


    def recommend_sim_cold_start(self, search):
        X, X_norm, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper, movies, _ = self.get_model()
        genres = set(g for G in movies['genres'] for g in G)
        for g in genres:
            movies[g] = movies.genres.transform(lambda x: int(g in x))

        movie_genres = movies.drop(columns=['movieId', 'title', 'genres'])
        cosine_sim = cosine_similarity(movie_genres, movie_genres)
        movie_idx = dict(zip(movies['title'], list(movies.index)))
        title = self.movie_finder(search, movies)[0]
        n_recommendations = 10

        idx = movie_idx[title]
        sim_scores = list(enumerate(cosine_sim[idx]))
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
        sim_scores = sim_scores[1:(n_recommendations + 1)]
        similar_movies = [i[0] for i in sim_scores]

        print(f"Recommendations for {title}:")
        print(movies['title'].iloc[similar_movies])
        return movies['title'].iloc[similar_movies]

    def recommend_sim_cold_start_rand(self):
        X, X_norm, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper, movies, ratings = self.get_model()
        movie_stats = ratings.groupby('movieId')[['rating']].agg(['count', 'mean'])
        movie_stats.columns = movie_stats.columns.droplevel()

        C = movie_stats['count'].mean()
        m = movie_stats['mean'].mean()

        def bayesian_avg(ratings):
            bayesian_avg = (C * m + ratings.sum()) / (C + ratings.count())
            return bayesian_avg

        bayesian_avg_ratings = ratings.groupby('movieId')['rating'].agg(bayesian_avg).reset_index()
        bayesian_avg_ratings.columns = ['movieId', 'bayesian_avg']
        movie_stats = movie_stats.merge(bayesian_avg_ratings, on='movieId')

        movie_stats = movie_stats.merge(movies[['movieId', 'title']])
        movie_stats = movie_stats.sort_values('bayesian_avg', ascending=False)
        top50 = movie_stats[:50]
        randk = random.randrange(50)

        genres = set(g for G in movies['genres'] for g in G)
        for g in genres:
            movies[g] = movies.genres.transform(lambda x: int(g in x))

        movie_genres = movies.drop(columns=['movieId', 'title', 'genres'])
        cosine_sim = cosine_similarity(movie_genres, movie_genres)
        movie_idx = dict(zip(movies['title'], list(movies.index)))
        title = top50.iloc[randk].title
        n_recommendations = 10

        idx = movie_idx[title]
        sim_scores = list(enumerate(cosine_sim[idx]))
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
        sim_scores = sim_scores[1:(n_recommendations + 1)]
        similar_movies = [i[0] for i in sim_scores]

        print(f"Recommendations for {title}:")
        print(movies['title'].iloc[similar_movies])
        return movies['title'].iloc[similar_movies]


    def recommend_sim_MF(self, movie_id):
        movie_names = []
        X, X_norm, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper, movies, _ = self.get_model()
        movie_titles = dict(zip(movies['movieId'], movies['title']))
        svd = TruncatedSVD(n_components=20, n_iter=10)
        Z = svd.fit_transform(X.T)
        similar_movies = self.find_similar_movies(movie_id, Z.T, movie_mapper, movie_inv_mapper, metric='cosine', k=10)
        movie_title = movie_titles[movie_id]

        print(f"***Because you watched {movie_title}:")
        for i in similar_movies:
            print(movie_titles[i])
            movie_names.append(movie_titles[i])
        return movie_names


    def recommend_sim_MF_for_user(self, userId):
        top_N = 10
        movie_names = []
        X, X_norm, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper, movies, _ = self.get_model()
        movie_titles = dict(zip(movies['movieId'], movies['title']))
        svd = TruncatedSVD(n_components=20, n_iter=10)
        Z = svd.fit_transform(X.T)
        new_X = svd.inverse_transform(Z).T
        top_N_indices = new_X[user_mapper[userId]].argsort()[-top_N:][::-1]

        print(f"*** Top {top_N} Recommendations for UserId {userId}:")
        for i in top_N_indices:
            movie_id = movie_inv_mapper[i]
            print(movie_titles[movie_id])
            movie_names.append(movie_titles[movie_id])
        return movie_names
