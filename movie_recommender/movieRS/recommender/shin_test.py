import re
from itertools import permutations
def gather_substring(str):
    # res = [test_str[i: j] for i in range(len(test_str))
    #       for j in range(i + 1, len(test_str) + 1)]
    res = [''.join(p) for p in permutations(str)]
    return set(res)

def is_conseq(word):
    word = word.upper()
    for i in range(0,len(word)-1):
        if (ord(word[i]) + 1) == ord(word[i+1]):
            return True
    return False

def is_vowel_conseq(word):
    pattern = r"\b(?=[a-z]*[aeiou]{2})[a-z]+\b"
    result = re.findall(pattern, word, re.IGNORECASE)
    
    # vowels = "aeiouAEIOU"
    # result = [x for x in word.split() if any(len(set(x[i:i+2]).intersection(vowels))==  2 for i in range(len(x)))]

    # regex = re.search("[aeiou][aeiou]", word)
    # if regex:
    #     return True

    # print(result)
    if len(result) == 0:
        return False
    return True

def is_consonant_conseq(word):
    pattern = r"\b(?=[a-z]*[bcdfghjklmnpqrstvwxyz]{2})[a-z]+\b"
    result = re.findall(pattern, word, re.IGNORECASE)
    # print(result)

    if len(result) == 0:
        return False
    return True

def is_start_with_consonant(word):
    vowel = 'aeiou'
    if word[0].lower() in vowel:
        return False
    else:
        return True

def isValid(str):
    if not isConseq(str) or not is_vowel_conseq(str) or not is_consonant_conseq(str):
        return False
    

    # result = []

    # for i in range(0, len(str)):
    #     j = i + 1
    #     while j < len(str) + 1:
    #         result.append(str[i:j])
    # return result

# def createDupArray(string, size):
#     dupArray = []
 
#     # One by one copy words from the given wordArray
#     # to dupArray
#     for i in range(size):
#         dupArray.append(Word(string[i], i))
 
#     return dupArray


if __name__ == "__main__":
    result = gather_substring("AAAB")
    count = 0
    for str in result:
        if (not is_vowel_conseq(str)) and (not is_consonant_conseq(str)) and is_start_with_consonant(str):
            # if not is_consonant_conseq(str):
            # final_result.append(str)
            print(f"Valid {str}")
            count = count + 1
    print(f"Count: {count}")

    # print(set(final_result))
    # result = 0
    # N = 123406980
    # enable_print = N % 10
    # while N > 0:
    #     if enable_print == 0 and N % 10 != 0:
    #         enable_print = 1
    #     if enable_print == 1:
    #         print(N % 10, end="")
        # N = N // 10
    # while num > 0:
    #     remainder = num % 10
    #     result = (result * 10) + remainder
    #     num = num // 10
    
    # print(f"Reverse: {result}")