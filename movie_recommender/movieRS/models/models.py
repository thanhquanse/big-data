from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Movie(models.Model):
    movie_id = models.CharField(max_length=100, primary_key=True)
    title = models.CharField(max_length=255)
    genres = models.CharField(max_length=100)
    path = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.title


class User(models.Model):
    user_id = models.CharField(max_length=100, primary_key=True)
    username = models.CharField(max_length=100)
    full_name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.full_name


class Rating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    rating = models.FloatField(default=1, validators=[MaxValueValidator(5.0), MinValueValidator(0.0)])
