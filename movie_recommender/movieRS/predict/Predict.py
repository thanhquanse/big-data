# CometML for experiment logging


# Import packages
import pandas as pd
from sklearn.model_selection import train_test_split
import tensorflow as tf
import os
# Import utility scripts
from .data_processor import preprocess_data, preprocess_one_user_rating
# from model import Deep_AE_model
# from .utils import show_error, show_rmse, masked_rmse, masked_rmse_clip
from tensorflow.keras.models import load_model
import numpy as np

BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
movies_data = os.path.join(BASE_DIR, "predict/data/ml1m_movies.csv")
h5_model = os.path.join(BASE_DIR, "predict/model.h5")
rating_data = os.path.join(BASE_DIR, "predict/data/ml1m_ratings.csv")

# Load the ratings data
df = pd.read_csv(rating_data, sep='\t', encoding='latin-1',
                 usecols=['user_emb_id', 'movie_emb_id', 'rating', 'timestamp'])

movie_df = pd.read_csv(movies_data, sep='\t', encoding='latin-1')

# +1 is the real size, as they are zero based
num_users = df['user_emb_id'].unique().max() + 1
num_movies = df['movie_emb_id'].unique().max() + 1

# load model
Deep_AE = load_model(h5_model, compile=False)


def recommend_movie(user_id, top_ranking=10):
    # 1. get all history rating of user
    # 2. convert to nomal input model
    # 3. predict
    # 4. get top 10 movies id ranking
    user_rating_filter = df['user_emb_id'] == user_id

    users_items_matrix_train_zero = preprocess_one_user_rating(df[user_rating_filter], num_movies, 0)
    users_items_matrix_train_zero = tf.convert_to_tensor(users_items_matrix_train_zero, dtype=tf.float32)

    # Predict the model on test set
    print(users_items_matrix_train_zero.shape)
    predict_deep = Deep_AE.predict(users_items_matrix_train_zero)

    result_predict = np.array(predict_deep[0])

    top_n_ranking_id = (-result_predict).argsort()[0:top_ranking].tolist()
    top_n_ranking_value = [result_predict[rank] for rank in top_n_ranking_id]

    movie_list = []
    for idx in top_n_ranking_id:
        title = movie_df.loc[movie_df['movie_emb_id'] == idx].title
        genre = movie_df.loc[movie_df['movie_emb_id'] == idx].genre
        movie_list.append(title + '#' + genre)
    return movie_list


def get_rated_movies(user_id):
    movie_id_list = []
    movie_name_list = []
    movie_genre_list = []
    movie_dict_list = []
    movie_rating_list = []

    movie_id_list = df.loc[df['user_emb_id'] == user_id].movie_emb_id.tolist()
    
    if len(movie_id_list) > 50:
        loop = 50
    for i in range(loop):
        movie_name_list.append(movie_df.loc[movie_df['movie_emb_id'] == movie_id_list[i]].title.item())
        movie_genre_list.append(movie_df.loc[movie_df['movie_emb_id'] == movie_id_list[i]].genre.item())
        movie_rating_list.append(df.loc[(df['user_emb_id'] == user_id) & (df['movie_emb_id'] == movie_id_list[i])].rating.item())
    for j in range(len(movie_name_list)):
        movie_dict_list.append(
            {
                "user_id": user_id,
                "movie_id": movie_id_list[j],
                "genre": movie_genre_list[j],
                "movie_title": movie_name_list[j],
                "rating": movie_rating_list[j]
            }
            )

    return movie_dict_list
