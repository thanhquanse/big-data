import json
from kafka import KafkaProducer


class MovieRSProducer:
    def __init__(self, bootstrap_server="localhost:9092", topic="MovieRS"):
        self.bootstrap_server = bootstrap_server
        self.topic = topic
        self.producer = KafkaProducer(bootstrap_servers=self.bootstrap_server, value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    def produce_message(self, mgs):
        future = self.producer.send(self.topic, mgs)
        print(f'Sending message: {mgs}')
        result = future.get(timeout=60)
        print(f'Result of sending message: {result}')
        metrics = self.producer.metrics()


if __name__ == "__main__":
    Producer = MovieRSProducer(bootstrap_server="35.225.134.239:9092", topic="MovieRSRequest")
    for i in range(10):
        Producer.produce_message(str(i))
