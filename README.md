# Big Data

**Authors**: Quan, Duong, Loc, Min

**Description**: This project is to research, apply big data processing techniques and develop a simple particular application to
deeply comprehend the strengths of the techniques/algorithms used in big data era

**Project scope**
- Data source:  https://www.kaggle.com/grouplens/movielens-20m-dataset?select=rating.csv
- Responsibility: Using the .csv data to build a recommender system to recommend viewers what films they may be interested in.
Focus on the accuracy and the speed of processing data. A simple UX/UI can be accepted
- Input: CSV dataset
- Output: List of films suggested for specific groups of users

**Relevant techniques/frameworks**
- Python 3.x
- Spark
- HDFS
- To be updated more while working...

**Big Data Algorithm**
- Dimensionality Reduction (SVD & CUR)

**ML & DL Algorithm**
- Matrix Factorization
- Deep Learning approach for Recommender Systems (RNN, LSTM, GRU..)

**Project duration**
- Duration: 5 weeks
- Start:    April 3rd
- End:      May 8th

**Project activities**
- Task assignment:  track in Gitlab ticket
- Milestone:        track in Gitlab milestone
- Code review:      track in Gitlab Merge Request
- Status update:    At any time
- Team gathering:   To be defined/flexible

**Extras**
- See how2run file to know more details about setting up server ENV
- W.r.t run code only, no need to read how2run, just follow the following

1. Install python 3 libs defined in `pip3 install -r lib2install.txt`
2. Run command `python3 consumer.py`
3. 
- Send user ID to get recommended films to "MovieRSRequest" topic
	Run `python3 src/producer.py <user_id>`

- Get list of movies in "MovieRSResponse" topic by opening a terminal where kafka is located
	`./kafka-console-consumer.sh --bootstrap-server 35.225.134.239:9092 --topic "MovieRSResponse"`


*To install frontend*

`cd frontend && npm install`

*To run dev frontend*

`cd frontend && npm start`

*To build frontend*

`cd frontend && npm run build`