export function translate(text) {
  const dict = {
    APP_TITLE: 'FILM LOVER',
    APP_FOOTER: 'Smart Team©2021',
  }
  return dict[text] || text;
}
export function capitalizeWords(string) {
  return string.replace(/(?:^|\s)\S/g, a => a.toUpperCase());
}