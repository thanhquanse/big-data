import React, { Component, Fragment } from "react";
import {
  Input,
  Typography,
} from "antd";

const { Title } = Typography;
const { Search } = Input;

export default class Comp extends Component {
  onSearch = (query) => {
    console.log(query);
    this.props.history.push(`/app/dashboard?search=${query}`);
  };

  render() {
    const { user } = this.props;

    return (
      <Fragment>
        {user && (
          <Title level={3}>
            <img
              className="border-radius-50-per"
              alt="user-avatar"
              src={`https://picsum.photos/50/50`}
            />
            &nbsp; Welcome {user.full_name},
          </Title>
        )}
        <div style={{ margin: "20px 0" }}>
          <Search
            placeholder="Enter a Film Name to Seach"
            onSearch={this.onSearch}
          />
        </div>
      </Fragment>
    );
  }
}
