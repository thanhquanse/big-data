import React, { Component } from "react";
import { Layout, Menu, Icon } from "antd";
const { Sider } = Layout;

export default class Comp extends Component {
  state = {
    collapsed: true,
  };

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  onSelect = (event) => {
    console.log(event);
    this.props.history.push(event.key);
  };

  render() {
    return (
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
      >
        <div className="logo" />
        <Menu
          onSelect={this.onSelect}
          theme="dark"
          defaultSelectedKeys={["/app/dashboard"]}
          mode="horizontal"
        >
          <Menu.Item key="/app/dashboard">
            <Icon type="home" />
            <span>Home</span>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}
