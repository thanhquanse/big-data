import React, { Component, Fragment } from 'react';
import { Spin } from 'antd';
import axios from 'axios';

class Comp extends Component {
  state = {
    data: null,
    pending: false,
    error: null,
  }

  excute = async () => {
    const endpoint = process.env.REACT_APP_API_HOST;
    const { method, url, payload } = this.props;
    this.setState({ pending: true })
    try {
      const response = await axios({
        method: method || 'get',
        url: endpoint + url,
        data: payload
      });
      this.setState({ data: response.data, pending: false, error: null });
      return response.data;
    } catch (error) {
      this.setState({ data:null, error, pending: false });
      return error;
    }
  }

  componentDidMount(){
    this.excute();
  }

  render() {
    if (this.state.pending) {
      return <Spin />
    }
    return(
      <Fragment>
        {this.props.children && this.props.children(
          this.state, () => this.excute(), this.state.error
        )}
      </Fragment>
    )
  }
}

export default Comp;