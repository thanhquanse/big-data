import React, { Component } from "react";
import { Menu } from "antd";
import { translate } from "../utils";

export default class Comp extends Component {
  onSelect = (event) => {
    console.log(event);
    if (event.key === 'logout') {
      localStorage.clear();
      this.props.history.push("/");
      return;
    }
    this.props.history.push(event.key);
  };

  render() {
    return (
      <Menu
        style={{ lineHeight: "64px" }}
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={["/app/dashboard"]}
        selectedKeys={[this.props.location.pathname]}
        onSelect={this.onSelect}
      >
        <Menu.Item key="/app">
          <img src="/logo.svg" height={40} style={{ marginTop: -10 }} alt="logo"/>
          &nbsp;<span className="app-title">{translate('APP_TITLE')}</span>
        </Menu.Item>
        <Menu.Item key="/app/dashboard">Home</Menu.Item>
        {(localStorage.getItem("username") === 'admin') && (
          <Menu.Item key="/app/admin">Admin</Menu.Item>
        )}
        <Menu.Item className="float-right" key="logout">
          Sign Out
        </Menu.Item>
        <Menu.Item className="float-right" key="information" disabled>
          Hello {localStorage.getItem("username")}
        </Menu.Item>
      </Menu>
    );
  }
}
