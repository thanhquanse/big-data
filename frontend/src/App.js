import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import LoginPage from './views/Login';
import SignupPage from './views/Signup';
import Layout from './views/Layout';
import 'antd/dist/antd.css';

class App extends Component {
  render() {
    return (
      <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
        <Switch>
          <Route exact path= "/" render={() => (
            localStorage.getItem('username') ? <Redirect to="/app"/> : <Redirect to="/login"/>
          )}/>
          <Route exact path='/login' component={LoginPage} />
          <Route exact path='/signup' component={SignupPage} />
          <Route path='/app' component={Layout} />
        </Switch>
      </div>
    </Router>
    );
  }
}

export default App;
