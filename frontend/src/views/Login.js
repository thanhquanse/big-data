import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox, Row, Col, Card } from 'antd';
import { translate } from "../utils";

class NormalLoginForm extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        localStorage.setItem('username', values.username);
        if (values.username === 'admin') {
          return this.props.history.push('/app/admin');
        }
        this.props.history.push('/');
      } 
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const leftCol = {
      lg: 8,
      md: 4,
      sm: 2,
      xs: 2,
    };
    const rightCol = {
      lg: 8,
      md: 16,
      sm: 20,
      xs: 20,
    };
    return (
      <Row className="background-image" style={{ minHeight: '100vh' }}>
        <Col {...leftCol}></Col>
        <Col {...rightCol}>
          <Card className="login-card filter">
            <h1 className="text-align-center">
              <img src="/logo.svg" height={40} style={{ marginTop: -20 }} alt="logo"/>
              &nbsp;<span className="app-title">{translate('APP_TITLE')}</span>
            </h1>
            <p className="subtitle text-align-center">LOGIN</p>
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item>
                {getFieldDecorator('username', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="Username"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                  <Input
                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type="password"
                    placeholder="Password"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                <p>
                  {getFieldDecorator('remember', {
                    valuePropName: 'checked',
                    initialValue: true,
                  })(<Checkbox>Remember me</Checkbox>)}
                  {/* <a className="login-form-forgot" href="">
                    Forgot password
                  </a> */}
                </p>
                <p>
                  <Button block type="primary" htmlType="submit" className="login-form-button">
                    Log in
                  </Button>
                </p>
                <p>
                  Or <a href="/signup">register now!</a>
                </p>
              </Form.Item>
            </Form>
          </Card>
        </Col>
        <Col {...leftCol}></Col>
      </Row>
    );
  }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(NormalLoginForm);

export default WrappedNormalLoginForm;
