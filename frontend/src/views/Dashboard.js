import React, { Fragment } from "react";
import _ from "lodash";
import {
  Card,
  Row,
  Col,
  Icon,
  Table,
  Typography,
  Carousel,
  Tooltip,
} from "antd";
import Fetch from "../components/Fetch";
import SearchBar from "../components/SearchBar";

const { Title } = Typography;
const { Meta } = Card;
const contentStyle = {
  padding: "20px",
  height: "320px",
  color: "#fff",
  textAlign: "center",
  cursor: "pointer",
};
const makeContentStyle = (props) => ({ ...contentStyle, ...props });

class Page extends React.Component {
  openFilm = (name) => {
    this.props.history.push(`/app/watch?name=${name}`);
  }

  getTitle(str) {
    var array = str.toString().split('#');
    return array[0];
  }

  getGenre(str) {
    var array = str.toString().split('#');
    return array[1];
  }

  renderSuggestionList = (arr) => {
    const colProps = {
      lg: 4,
      md: 6,
      sm: 12,
      xs: 24,
    };
    return (
      <Row gutter={[16, 16]}>
        {arr.map((e, i) => (
          <Col key={e} {...colProps}>
            <Card
              hoverable
              actions={[
                <span
                  onClick={() => {
                    this.openFilm(e);
                  }}
                  style={{ color: "pink" }}
                >
                  <Icon type="heart" key="love" />
                </span>,
                <span style={{ color: "blue" }} onClick={() => {
                  this.openFilm(e);
                }}>
                  <Icon type="eye" key="watch" />
                </span>,
              ]}
              cover={
                <img
                  alt="film-poster"
                  src={`https://picsum.photos/200/200?t=${i}`}
                />
              }
            >
              
              <Tooltip placement="bottom" title={this.getTitle(e)}>
                <Meta title={this.getTitle(e)} description={this.getGenre(e)} />
              </Tooltip>
            </Card>
          </Col>
        ))}
      </Row>
    );
  };

  renderCarousel = (arr) => {
    if (!arr || !arr[2]) return "";
    const max = arr[2].length > 10 ? 10 : arr[2].length;
    const sliders = arr.filter((e, i) => i < max);
    return (
      <Carousel autoplay>
        {sliders.map((e, i) => (
          <div key={`image-${i}`}>
            <div
              style={makeContentStyle({
                background: "center center",
                backgroundRepat: "no-repeat",
                backgroundSize: "cover",
                backgroundImage: `url("https://picsum.photos/1024/768?t=${i}")`,
              })}
            >
              <p className="color-white">The Most Popular Films</p>
              <span
                style={{ cursor: 'pointer' }}
                onClick={() => {
                  this.openFilm(e);
                }}
              >
                <Title className="color-white">{e}</Title>
              </span>
              
            </div>
          </div>
        ))}
      </Carousel>
    );
  };

  renderSearchItems = (_str) => {
    const str = `${_str}`.trim();
    if (!str || str.length < 1) return "";
    return (
      <Fetch key={`search-${str}`} url={`api/v1/movies/cold_start/${str}`}>
        {(state) => {
          if (state && state.error) {
            return <Title level={3}>
              There is no Search Results for "<b>{str}</b>"
            </Title>
          }
          return (
            <Fragment>
              <Title level={3}>
                Search Results for "<b>{str}</b>"
              </Title>
              <Card>
                {state &&
                  state.data &&
                  state.data[0] &&
                  this.renderSuggestionList(state.data)}
              </Card>
            </Fragment>
          );
        }}
      </Fetch>
    );
  };

  renderSuggestionForUser = (user) => {
    return (
      <Fetch url={`api/v1/movies/recommended_for/${user.user_id}`}>
        {(state) => {
          return (
            state &&
              state.data &&
              state.data[0] &&
              this.renderSuggestionList(state.data)
          )
        }}
      </Fetch>
    )
  }

  renderSuggestionFromColdStart = () => {
    return (
      <Fetch url={`api/v1/movies/cold_start`}>
        {(state) => {
          return (
            state &&
              state.data &&
              state.data[0] &&
              this.renderCarousel(state.data)
          )
        }}
      </Fetch>
    )
  }

  renderRatedFilms = (user, movies) => {
    const moviesHashList = _.keyBy(movies, 'movie_id');
    return (
      <Fetch url={`api/v1/movies/rated/user/${user.user_id}`}>
        {(state) => {
          return <Fragment>
            <Title level={3}>
              Rated Films List
            </Title>
            <Card>
              <Table
                pagination={{ pageSize: 10 }}
                dataSource={state.data}
                columns={[
                  {
                    title: "Movie",
                    dataIndex: "movie_title",
                    key: "movie_id",
                    render: (record, movie) => <span
                      className="cursor-pointer"
                      onClick={() => this.openFilm(movie.title)}>
                        {record}</span>
                  },
                  {
                    title: "Genre",
                    dataIndex: "genre",
                    key: "genre",
                    render: (record, movie) => <p>
                      <span
                        onClick={() => {
                          this.openFilm(movie.title);
                        }}
                        style={{ color: "deeppink" }}
                      >
                        {record}
                      </span>
                    </p>
                  },
                  {
                    title: "Point",
                    dataIndex: "rating",
                    key: "rating",
                    render: (record, movie) => <p>
                      <span
                        onClick={() => {
                          this.openFilm(movie.title);
                        }}
                        style={{ color: "deeppink" }}
                      >
                        <Icon type="heart" key="love" /> {record}
                      </span>
                    </p>
                  }
                ]}
              />
            </Card>
          </Fragment>
        }}
      </Fetch> 
    )
  }

  renderUserOverview = (user) => {
    console.log(this.props);
    const searchStr = (this.props.location && this.props.location.search) || "";
    const query = searchStr.split("search=")[1] || false;

    return (
      <div className="card-list">
        <SearchBar {...this.props} user={user} />
        {query && this.renderSearchItems(query)}
        <Tooltip placement="bottomLeft" title={'Suggestion Films from Cold Start'}>
          <Title level={3}>Top Trending Film</Title>
        </Tooltip>
        <Card className="no-padding">
          {this.renderSuggestionFromColdStart()}
        </Card>
        <Tooltip placement="bottomLeft" title={'Suggestion Films for A Specific User'}>
          <Title level={3}>Maybe you love</Title>
        </Tooltip>
        <Card>
          {this.renderSuggestionForUser(user)}
        </Card>
        <Card>
          <Fetch url={`api/v1/movies`}>
            {(state) => {
              return (
                state &&
                state.data &&
                state.data[0] &&
                this.renderRatedFilms(user, state.data)
              );
            }}
          </Fetch>
        </Card>
      </div>
    );
  };

  render() {
    const username = localStorage.getItem("username");
    return (
      <Fetch url={`api/v1/users`}>
        {(state) => {
          const user =
            state &&
            state.data &&
            state.data[0] &&
            state.data.find((u) => u.username === username);
          if (!user) {
            return this.renderUserOverview({
              user_id: 1,
              full_name: "guess",
              username: "guess",
            });
          }
          return this.renderUserOverview(user);
        }}
      </Fetch>
    );
  }
}

export default Page;
