import React, { Component } from 'react';
import { Form, Icon, Input, Button, Row, Col, Card } from 'antd';
import { translate } from "../utils";
import axios from 'axios';

class NormalLoginForm extends Component {

  submit = async (values) => {
    const endpoint = process.env.REACT_APP_API_HOST;

    values.userID = (new Date()).getTime();

    const response = await axios({
      method: 'post',
      url: endpoint + 'api/v1/users/',
      data: values
    });

    const data = response.data;
    console.log(data);

    this.props.history.push('/login');
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.submit(values);        
      } 
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const leftCol = {
      lg: 8,
      md: 4,
      sm: 2,
      xs: 2,
    };
    const rightCol = {
      lg: 8,
      md: 16,
      sm: 20,
      xs: 20,
    };
    return (
      <Row className="background-image" style={{ minHeight: '100vh' }}>
        <Col {...leftCol}></Col>
        <Col {...rightCol}>
          <Card className="login-card filter">
            <h1 className="text-align-center">
              <img src="/logo.svg" height={40} style={{ marginTop: -20 }} alt="logo"/>
              &nbsp;<span className="app-title">{translate('APP_TITLE')}</span>
            </h1>
            <p className="subtitle text-align-center">SIGN UP</p>
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item>
                {getFieldDecorator('username', {
                  rules: [{ required: true, message: 'Please input your Username!' }],
                })(
                  <Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="Username"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('full_name', {
                  rules: [{ required: true, message: 'Please input your Fullname!' }],
                })(
                  <Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="Full Name"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                  <Input
                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type="password"
                    placeholder="Password"
                  />,
                )}
              </Form.Item>
              <Form.Item>
                <p>
                  <Button block type="primary" htmlType="submit" className="login-form-button">
                    Submit
                  </Button>
                </p>
                <p className="text-align-right">
                  <a href="/login">
                    Go back to login
                  </a>
                </p>
              </Form.Item>
            </Form>
          </Card>
        </Col>
        <Col {...leftCol}></Col>
      </Row>
    );
  }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(NormalLoginForm);

export default WrappedNormalLoginForm;
