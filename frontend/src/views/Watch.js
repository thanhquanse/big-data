import React, { Fragment } from "react";
import axios from 'axios';
import ReactStars from "react-rating-stars-component";
import {
  Card,
  Row,
  Col,
  Icon,
  Progress,
  Table,
  Button,
  Typography,
  Tooltip,
  Breadcrumb,
  notification,
} from "antd";
import Fetch from "../components/Fetch";
import SearchBar from "../components/SearchBar";

const { Title } = Typography;
const { Meta } = Card;

const RatingComponent = (props) => (<ReactStars
  key={props.point}
  count={5}
  value={props.value}
  onChange={props.onChange}
  size={24}
  isHalf={false}
  emptyIcon={<i className="far fa-star"></i>}
  halfIcon={<i className="fa fa-star-half-alt"></i>}
  fullIcon={<i className="fa fa-star"></i>}
  activeColor="#ffd700"
/>);

class Page extends React.Component {
  state = {sessionId: 0, value: 0}

  rateFilm = async (movie, user, rating) => {
    const endpoint = process.env.REACT_APP_API_HOST;
    const sessionId = (new Date()).getTime();
    console.log('user', user)
    const response = await axios({
      method: 'post',
      url: endpoint + 'api/v1/movies/ratings/update/',
      data: {
        'movieID': movie.movie_id,
        'userID': user.user_id,
        'rating': rating,
      }
    });

    const data = response.data;
    console.log(data);

    this.setState({ sessionId, value: rating });

    notification.success({
      message: `You just rated ${rating} stars for this film`,
    });
  }

  openFilm = (name) => {
    this.props.history.push(`/app/watch?name=${name}`);
  };

  renderSuggestionList = (arr) => {
    const colProps = {
      lg: 4,
      md: 6,
      sm: 12,
      xs: 24,
    };
    return (
      <Row gutter={[16, 16]}>
        {arr.map((e, i) => (
          <Col key={e} {...colProps}>
            <Card
              hoverable
              actions={[
                <span
                  style={{ color: "blue" }}
                  onClick={() => {
                    this.openFilm(e);
                  }}
                >
                  <Icon type="eye" key="watch" />
                </span>,
              ]}
              cover={
                <img
                  alt="film-poster"
                  src={`https://picsum.photos/200/200?t=${i}`}
                />
              }
            >
              <Tooltip placement="bottom" title={e}>
                <Meta title={e} description="Funny" />
              </Tooltip>
            </Card>
          </Col>
        ))}
      </Row>
    );
  };

  renderDetail = (movie, user) => {
    const leftCol = {
      lg: 4,
      md: 4,
      sm: 24,
      xs: 24,
    };
    const rightCol = {
      lg: 20,
      md: 20,
      sm: 24,
      xs: 24,
    };
    return (
      <Fragment>
        <Card key={this.state.sessionId}>
          <Row gutter={[16, 16]}>
            <Col {...leftCol}>
              <img
                style={{ width: "100%", borderRadius: '20%' }}
                alt="film-poster"
                src={`https://picsum.photos/320/320`}
              />
            </Col>
            <Col {...rightCol}>
              <p>Film #{movie.movieID}</p>
              <Title level={3}>{movie.title}</Title>
              <div>{movie.gender}</div>
              <p>
                <Fetch url={`api/v1/movies/ratings/${movie.movie_id}/${user.user_id}`}>
                  {(state) => {
                    const point = state.data || 0;
                    return (
                      <RatingComponent
                        point={point}
                        value={point}
                        onChange={(point) => {
                          this.rateFilm(movie, user, point);
                        }}
                      />
                    );
                  }}
                </Fetch>
              </p>
              <Fetch url={`api/v1/movies/ratings/avg/${movie.movie_id}`}>
                {(state) => {
                  return <p style={{ maxWidth: 320 }}>
                    <div>
                      <b>
                      Rating Point (Community Average): {((state.data - 0) || 0)}
                      </b>
                    </div>
                    <Progress
                      strokeColor={{
                        '0%': 'gray', //'#108ee9',
                        '100%': 'deeppink', //'#87d068',
                      }}
                      percent={Math.floor(((state.data - 0) || 0) * 100 / 5)}
                    />
                  </p>;
                }}
              </Fetch>
              <p>
                <Button icon="caret-right" type="primary">
                  Play Now
                </Button>
              </p>
              
            </Col>
          </Row>
        </Card>
        <Fetch url={`api/v1/movies/ratings/${movie.movie_id}`}>
          {(state) => {
            return <Fragment>
              <Title level={3}>
                Rating List
              </Title>
              <Card>
                <Table
                  pagination={{ pageSize: 3 }}
                  dataSource={state.data}
                  columns={[
                    {
                      title: "User Rating",
                      dataIndex: "user_id",
                      key: "id",
                      render: (record, movie) => <p>
                        #{record} {{
                          1: 'Bad',
                          1.5: 'Not Bad',
                          2: 'Normal',
                          2.5: 'Need Improve',
                          3: 'Ok',
                          3.5: 'Still Ok',
                          4: 'Good',
                          3.5: 'Still Good',
                          5: 'Excellent',
                        }[movie.rating] || 'Hmm'}
                      </p>
                    },
                    {
                      title: "Point",
                      dataIndex: "rating",
                      key: "rating",
                      render: (record) => <p>
                        <span
                          style={{ color: "deeppink" }}
                        >
                          <Icon type="heart" key="love" /> {record}
                        </span>
                      </p>
                    }
                  ]}
                />
              </Card>
            </Fragment>
          }}
        </Fetch> 
      </Fragment>
    );
  };

  renderItem = (movies, name, user) => {
    const foundMovie = movies.find((e) => `${e.title}`.includes(name));
    if (!foundMovie) {
      return (
        <Title level={3}>
          Not found film <b>{name}</b>
        </Title>
      );
    }
    return (
      <Fragment>
        {this.renderDetail(foundMovie, user)}
        <Fetch key={name} url={`api/v1/movies/watching/${foundMovie.movie_id}`}>
          {(state) => {
            return (
              state &&
              state.data &&
              state.data[0] && (
                <Fragment>
                  <Tooltip
                    placement="bottomLeft"
                    title={"Suggestion Similar Films"}
                  >
                    <Title level={3}>Similar Films</Title>
                  </Tooltip>
                  <Card>{this.renderSuggestionList(state.data)}</Card>
                </Fragment>
              )
            );
          }}
        </Fetch>
      </Fragment>
    );
  };

  renderUserOverview = (user) => {
    console.log(this.props);
    const searchStr = (this.props.location && this.props.location.search) || "";
    const query = decodeURI(searchStr.split("name=")[1] || "");
    console.log(query);
    const leftCol = {
      lg: 18,
      md: 18,
      sm: 24,
      xs: 24,
    };
    const rightCol = {
      lg: 6,
      md: 6,
      sm: 24,
      xs: 24,
    };
    return (
      <div className="card-list">
        <Row>
          <Col {...leftCol}>
            <Breadcrumb style={{ margin: "16px 0", lineHeight: "40px" }}>
              <a href="/app/dashboard">
                <Breadcrumb.Item>
                  <Icon type="home" /> Home
                </Breadcrumb.Item>
              </a>
              <Breadcrumb.Item>
                Watching Film <b>{query}</b>
              </Breadcrumb.Item>
            </Breadcrumb>
          </Col>
          <Col {...rightCol}>
            <SearchBar {...this.props} />
          </Col>
        </Row>
        <Fetch url={`api/v1/movies`}>
          {(state) => {
            return (
              state &&
              state.data &&
              state.data[0] &&
              this.renderItem(state.data, query, user)
            );
          }}
        </Fetch>
      </div>
    );
  };

  render() {
    const username = localStorage.getItem("username");
    return (
      <Fetch url={`api/v1/users`}>
        {(state) => {
          const user =
            state &&
            state.data &&
            state.data[0] &&
            state.data.find((u) => u.username === username);
          if (!user) {
            return this.renderUserOverview({
              user_id: 1,
              full_name: "guess",
              username: "guess",
            });
          }
          return this.renderUserOverview(user);
        }}
      </Fetch>
    );
  }
}

export default Page;
