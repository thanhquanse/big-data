import React from "react";
import { Layout } from "antd";
import { Switch, Route, Redirect } from "react-router-dom";
import DashboardPage from "./Dashboard";
import AdminPage from "./Admin";
import WatchPage from "./Watch";
import { translate } from "../utils";
import HeaderMenu from "../components/HeaderMenu";
// import SiderBar from "../components/SiderBar";

const { Header, Content, Footer } = Layout;

class LeftSider extends React.Component {
  render() {
    return (
      <Layout style={{ minHeight: "100vh" }}>
        {/* <SiderBar {...this.props} /> */}
        <Layout>
          <Header className="header" style={{ background: "#fff", padding: 0 }}>
            <HeaderMenu {...this.props} />
          </Header>
          <Content style={{ margin: "0 16px" }}>
            <div style={{ padding: 24, minHeight: 360 }}>
              <Switch>
                <Route
                  exact
                  path="/app"
                  render={() => <Redirect to="/app/dashboard" />}
                />
                <Route exact path="/app/dashboard" component={DashboardPage} />
                <Route exact path="/app/watch" component={WatchPage} />
                <Route exact path="/app/admin" component={AdminPage} />
              </Switch>
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            {translate("APP_FOOTER")}
          </Footer>
        </Layout>
      </Layout>
    );
  }
}

export default LeftSider;
