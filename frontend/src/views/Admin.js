import React, { Fragment } from "react";
import {
  Card,
  Icon,
  Button,
  Typography,
  Breadcrumb,
  Table,
  Input,
} from "antd";
import Fetch from "../components/Fetch";
import axios from 'axios';

const { Title } = Typography;

class Page extends React.Component {
  state = {
    title: '',
    genre: '',
  }
  openFilm = (name) => {
    this.props.history.push(`/app/watch?name=${name}`);
  };

  renderMoviesList = () => {
    return (
      <Fragment>
        <Title level={3}>Movies List</Title>
        <p>
          <Input
            placeholder="Enter film name"
            style={{ width: 200 }}
            value={this.state.title}
            onChange={(evt) => this.setState({ title: evt.target.value })}
          /> &nbsp;
          <Input
            placeholder="Enter film genre"
            style={{ width: 200 }}
            value={this.state.genre}
            onChange={(evt) => this.setState({ genre: evt.target.value })}
          /> &nbsp;
          <Button onClick={async () => {
            const endpoint = process.env.REACT_APP_API_HOST;
            const values = {
              movieID:(new Date()).getTime(),
              title: this.state.title,
              genre: this.state.genre,
            }
        
            const response = await axios({
              method: 'post',
              url: endpoint + 'api/v1/movies/',
              data: values
            });
        
            const data = response.data;
            console.log(data);
            this.setState({ title: '', genre: '', sessionId: (new Date()).getTime() })
          }}
          icon="plus">Add</Button>
        </p>
        <Fetch key={this.state.sessionId} url={`api/v1/movies`}>
          {(state) => {
            return (
              <Card>
                <Table
                  dataSource={state.data}
                  columns={[
                    {
                      title: "ID",
                      dataIndex: "movie_id",
                      key: "id",
                    },
                    {
                      title: "Title",
                      dataIndex: "title",
                      key: "title",
                    },
                    {
                      title: "Genre",
                      dataIndex: "genre",
                      key: "genre",
                    },
                    {
                      title: "Actions",
                      dataIndex: "user_id",
                      key: "actions",
                      render: (index, record) => {
                        return (
                          <Fragment>
                            <Button
                              icon="eye"
                              onClick={() => {
                                this.openFilm(record.title);
                              }}
                            >
                              View
                            </Button>
                          </Fragment>
                        );
                      },
                    },
                  ]}
                />
              </Card>
            );
          }}
        </Fetch>
      </Fragment>
    );
  };

  renderUsersList = () => {
    return (
      <Fragment>
        <Title level={3}>Users List</Title>
        <Fetch url={`api/v1/users`}>
          {(state) => {
            return (
              <Card>
                <Table
                  dataSource={state.data}
                  columns={[
                    {
                      title: "ID",
                      dataIndex: "user_id",
                      key: "id",
                    },
                    {
                      title: "Username",
                      dataIndex: "username",
                      key: "username",
                    },
                    {
                      title: "Actions",
                      dataIndex: "user_id",
                      key: "actions",
                      render: (index, record) => {
                        return (
                          <Fragment>
                            <Button
                              icon="caret-right"
                              onClick={() => {
                                localStorage.setItem(
                                  "username",
                                  record.username
                                );
                                this.props.history.push("/");
                              }}
                            >
                              Login As
                            </Button>
                          </Fragment>
                        );
                      },
                    },
                  ]}
                />
              </Card>
            );
          }}
        </Fetch>
      </Fragment>
    );
  };

  render() {
    const username = localStorage.getItem("username");
    if (username !== "admin")
      return "Sorry! You don't have permission to see this page";

    return (
      <Fragment>
        <Breadcrumb style={{ margin: "16px 0", lineHeight: "40px" }}>
          <a href="/app/dashboard">
            <Breadcrumb.Item>
              <Icon type="home" /> Home
            </Breadcrumb.Item>
          </a>
          <Breadcrumb.Item>Admin Page</Breadcrumb.Item>
        </Breadcrumb>
        {this.renderMoviesList()}
        <br />
        {this.renderUsersList()}
      </Fragment>
    );
  }
}

export default Page;
